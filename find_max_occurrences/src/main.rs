// use lambda_runtime::{handler_fn, Context, Error};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};


use serde::{Deserialize, Serialize};
use std::collections::HashMap;


#[derive(Deserialize, Serialize)]
struct Counts {
    upper_case: usize,
    lower_case: usize,
    numbers: usize,
    others: usize,
}


#[derive(Deserialize, Serialize)]
struct Input {
    counts: Counts,
    original_text: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    most_frequent: String,
    original_text: String,
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(find_max_occurrences);
    run(func).await?;
    Ok(())
}

async fn find_max_occurrences(input: LambdaEvent<Input>) -> Result<Output, &'static str> {
    let mut counts = HashMap::new();
    let input_val = input.payload.counts;
    counts.insert("upper_case".to_string(), input_val.upper_case);
    counts.insert("lower_case".to_string(), input_val.lower_case);
    counts.insert("numbers".to_string(), input_val.numbers);
    counts.insert("others".to_string(), input_val.others);

    let most_frequent = counts.into_iter().max_by_key(|&(_, count)| count).map(|(kind, _)| kind).unwrap_or_else(|| "None".to_string());

    Ok(Output {
        most_frequent,
        original_text: input.payload.original_text,
    })
}

