# Rust AWS Lambda and Step Functions Practice
This project is going to develope a text data processing pipeline using two orchestrated lambda function: the `count_characters` function counts the main 4 type of characters in the input sentence and the `find_max_occurrences` function finds the max frequency word from the statistical result of last step. The two lambda functions are coordinated in the State Machine of AWS Step Funtions.

## Demo Video
https://youtu.be/5jeev7AByME

## Goals
* Rust AWS Lambda function
* Step Functions workflow coordinating Lambdas
* Orchestrate data processing pipeline

## Preparation
1. Prepare for the environment.
```shell
sudo apt install python3-pip
python3 -m venv venv
source ./venv/bin/activate
```
2. PiP on any system with Python 3 installed
`pip3 install cargo-lambda`

* [See other installation options](https://github.com/awslabs/aws-lambda-rust-runtime)

3. Ready with the right version of Rust
You can use `rustup default stable` and `rustc --version` to check if your rust version meet the requirements of 1.72.0.

4. Configue the AWS credentials for the extension. 

4. Read the [documents](https://www.cargo-lambda.info/guide/getting-started.html) if you are not familar with Cargo Lambda commands.


## Steps
### Step 1: Create Lambda Functions
1. Initializing new AWS Lambda project in Rust using command line `cargo lambda new <PROJECT_NAME>` in terminal.
```
cargo lambda new count_characters
```
```
cargo lambda new find_max_occurrences
```
2. Add necessary dependencies to `Cargo.toml` file.
3. Add functional implementations and inference endpoint in `main.rs` file.
4. Create the .json files and put at their corresponding project directory for local testing:
* input.json
```
{"data": "The brown f12 34 !."}
```
* input2.json
```
{
    "counts": {
        "upper_case": 1,
        "lower_case": 8,
        "numbers": 4,
        "others": 2
    },
    "original_text": "The brown f12 34 !."
}
```
5. Test these two lambda functions locally by running:
```
cd count_characters
cargo lambda watch
cargo lambda invoke --data-file input.json
```
```
cd find_max_occurrences
cargo lambda watch
cargo lambda invoke --data-file input2.json
```
- Local test
![alt text](images/image.png)
![alt text](images/image-1.png)
![alt text](images/image-2.png)

### Step 2: Deploy Lambda Functions to AWS
1. After succesfully testing the lambda functions, push these functions to the Gitlab.
2. Set the AWS access variable in `Settings` -> `CI/CD`.
3. Add the `.gitlab-ci.yml` file. Then, it will automatically deploy the lambda functions to AWS.
![alt text](images/image-4.png)
![alt text](images/image-3.png)

### Step 3: Orchestrate Step Functions Pipeline
1. Open the AWS Management Console and navigate to the AWS Step Functions page.
2. Create a new state machine with blank template in AWS Step Functions. Choose `AWS Lambda Invoke`. Define the state machine as follows:
![alt text](images/image-5.png)
![alt text](images/image-6.png)

3. Test your workflow. Click `Start Execution` to test the pipeline with the input: 
```json
{"data": "The brown f12 34 !."}
```
![alt text](images/image-7.png)
![alt text](images/image-8.png)
![alt text](images/image-9.png)

