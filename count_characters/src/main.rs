use std::collections::HashMap;

// use lambda_runtime::{handler_fn, Context, Error};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
struct Input {
    data: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    counts: Counts,
    original_text: String,
}

#[derive(Deserialize, Serialize)]
struct Counts {
    upper_case: usize,
    lower_case: usize,
    numbers: usize,
    others: usize,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(count_characters);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn count_characters(event: LambdaEvent<Input>) -> Result<Output, Error> {
    let mut upper_case = 0;
    let mut lower_case = 0;
    let mut numbers = 0;
    let mut others = 0;

    for character in event.payload.data.chars() {
        if character.is_uppercase() {
            upper_case += 1;
        } else if character.is_lowercase() {
            lower_case += 1;
        } else if character.is_numeric() {
            numbers += 1;
        } else if character.is_whitespace() {
            continue;
        } else {
            others += 1;
        }
    }
    let counts = Counts {
        upper_case,
        lower_case,
        numbers,
        others,
    };

    Ok(Output {
        counts,
        original_text: event.payload.data,
    })

}

